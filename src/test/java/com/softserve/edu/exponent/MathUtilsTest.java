package com.softserve.edu.exponent;

import static org.junit.Assert.*;
import static com.softserve.edu.exponent.MathUtils.*;
import org.junit.Test;

public class MathUtilsTest {

    @Test
    public void powTest10_6() {

        assertEquals(Math.pow(10, 6), MathUtils.pow(10,6), 0.001);

    }

    @Test
    public void powTestMinus10_2() {
        assertEquals(Math.pow(10, -3), MathUtils.pow(10,-3), 0.001);
    }

    @Test
    public void powTest0() {
        assertEquals(1, MathUtils.pow(10,0), 0.0001);
    }

}