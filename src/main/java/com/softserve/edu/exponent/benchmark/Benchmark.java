package com.softserve.edu.exponent.benchmark;

import com.softserve.edu.exponent.MathUtils;

public class Benchmark {

    public static void compareAlgorithms(double a, double b) {
        long start = System.nanoTime();
        Math.pow(a,b);
        long end = System.nanoTime();
        long timeFirst = end - start;
        start = System.nanoTime();
        MathUtils.pow(a,b);
        end = System.nanoTime();
        long secondTime = end - start;
        System.out.println("======== Benchmark results ==========");
        System.out.format("Math.pow time %d\n", timeFirst);
        System.out.format("My implementation time time %d\n", secondTime);
        System.out.format("Math.pow is %d times faster\n", secondTime / timeFirst);
    }

}
