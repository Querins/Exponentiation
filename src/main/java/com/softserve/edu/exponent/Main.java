package com.softserve.edu.exponent;

import com.softserve.edu.exponent.benchmark.Benchmark;

public class Main {

    public static void main(String[] args) {

        switch (args.length) {

            case 0:
                printInstructions();
                break;
            case 1:
                printInstructions();
                break;
            default:
                try {
                    double a = Double.parseDouble(args[0]);
                    double b = Double.parseDouble(args[1]);
                    Benchmark.compareAlgorithms(a,b);
                } catch (NumberFormatException e) {
                    System.out.println("Wrong parameters");
                }

        }

    }

    private static void printInstructions() {

        System.out.println("a^b a and be are passed through command line parameters");

    }

}
