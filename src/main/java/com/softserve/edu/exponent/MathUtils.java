package com.softserve.edu.exponent;

public class MathUtils {

    public static double pow(double a, double pow) {

        if(pow < 0) {
            return 1 / pow(a, -pow);
        }
        int result = 1;
        while (pow > 0) {
            if (pow % 2 == 1) {
                result *= a;
            }
            a *= a;
            pow /= 2;
        }
        return result;

    }

}